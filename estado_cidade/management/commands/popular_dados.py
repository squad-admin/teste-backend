from django.core.management.base import BaseCommand
from estado_cidade.models import Estado, Cidade

class Command(BaseCommand):

    help = "add example job"
    data = [
        ("São Paulo", "SP", [
            "São Paulo",
            "Sorocaba",
            "Jundiaí",
            "Itú",
            "Campinas"
        ]),
        ("Rio de Janeiro", "RJ", [
            "Rio de Janeiro",
            "Cabo Frio",
            "Niterói",
        ]),
        ("Minas Gerais", "MG", [
            "Belo Horizonte",
        ]),
        ("Paraná", "PA", [
            "Curitiba",
            "Cáscavel",
            "Londrina",
        ]),
        ("Santa Catarina", "SC", [
            "Florianópolis",
        ]),
    ]

    def handle(self, *args, **options):
        Cidade.objects.all().delete() # clean current data
        Estado.objects.all().delete() # clean current data'
        self.stdout.write("Dados antigos removidos")
        for estado_data in self.data:
            estado = Estado.objects.create(nome=estado_data[0], sigla=estado_data[1])
            self.stdout.write("Estado criado: " + estado.nome)
            for cidade_nome in estado_data[2]:
                Cidade.objects.create(estado=estado, nome=cidade_nome)
                self.stdout.write("Cidade criada: " + cidade_nome + " para estado " + estado.nome)