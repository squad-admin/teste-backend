from rest_framework.views import APIView
from rest_framework.exceptions import ValidationError
from rest_framework.response import Response


class ListarEstadosAPIView(APIView):

    """

    Especificação do endpoint

    [GET /estados/]

    Response 200:
    {
        "estados": [
            {
                "nome": "São Paulo",
                "id": 1,
                "sigla": "SP",
                cidades: [
                    {
                        "nome": "São Paulo",
                        "id": 1,
                    },
                    ...
                ]
            },
            {
                "nome": "Santa Catarina",
                "id": 2,
                "sigla": "SC",
                cidades: [
                    {
                        "nome": "Florianópolis",
                        "id": 2,
                    },
                    ...
                ]
            },
            ...
        ]
    }

    """

    def get(self, request, *args, **kwargs):
        return Response(status=400)


class CidadesDeUmEstadoAPIView(APIView):

    """

    Especificação do endpoint

    [POST /estado/cidades/]

    Request:
    {
        "sigla": "sp"
    }

    Response 200:
    {
        "cidades": [
            {
                "nome": "São Paulo",
                "id": 1
            },
            ...
        ]
    }

    """

    def post(self, request, *args, **kwargs):
        return Response(status=400)


class AutocompleteCidadeAPIView(APIView):

    """

    Especificação do endpoint

    [POST /cidade/autocomplete/]

    Request:
    {
        "text": "S"
    }

    Response 200:
    {
        "cidades": [
            {
                "nome": "São Paulo",
                "id": 1
            },
            {
                "nome": "Sorocaba",
                "id": 1
            },
        ]
    }

    ou

    Request:
    {
        "text": "Lond"
    }

    Response 200:
    {
        "cidades": [
            {
                "nome": "Londrina",
                "id": 1
            },
            ...
        ]
    }

    """

    def post(self, request, *args, **kwargs):
        return Response(status=400)